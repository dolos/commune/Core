//
// Created by mathias on 06.11.18.
//

#ifndef CORE_EVENTTEST_HPP
#define CORE_EVENTTEST_HPP

namespace test
{
    class EventTest
    {
    public:
        EventTest() = default;
        EventTest(int x) { setInt(x); }
        EventTest(const EventTest&) = default;
        EventTest &operator=(const EventTest&) = default;
        ~EventTest() = default;

    public:
        int getInt() { return (_x); }
        EventTest &setInt(int x) { _x = x; return *this; }

    private:
        int _x = 0;
    };
}

#endif //CORE_EVENTTEST_HPP
