/*
** EPITECH PROJECT, 2018
** Core
** File  description:
**
*/

#ifndef CORE_DOLOSRESSOURCESMANAGER_HPP
#define CORE_DOLOSRESSOURCESMANAGER_HPP

#include <list>
#include <mutex>
#include <thread>
#include <atomic>
#include <condition_variable>
#include "Dolos_SDK/IRessourcesManager.hpp"

# define RESSOURCES_MANAGER_NAME "RessourcesManager"
# define RESSOURCES_PATH_MAX_LENGTH 200

namespace dolos
{
    class DolosRessourcesManager : public IRessourcesManager, public IModule {
    public:
        DolosRessourcesManager();
        DolosRessourcesManager(const DolosRessourcesManager&) = delete;
        DolosRessourcesManager(const DolosRessourcesManager&&) = delete;
        DolosRessourcesManager &operator=(const DolosRessourcesManager&) = delete;
        DolosRessourcesManager &operator=(const DolosRessourcesManager&&) = delete;
        virtual ~DolosRessourcesManager();

    // IRessourcesManager implementation
    public:
	    AccessResult accessRead(const std::string &path,
	                            const OnReadAccess &onReadAccess,
	                            std::ios_base::openmode mode) override;
	    AccessResult accessWrite(const std::string &path,
	                             const OnWriteAccess &onWriteAccess,
	                             std::ios_base::openmode mode) override;

	    void asyncAccessRead(const std::string &path,
	                         const OnAsyncReadAccess &onReadAccess,
	                         std::ios_base::openmode mode) override;
	    void asyncAccessWrite(const std::string &path,
	                          const OnAsyncWriteAccess &onWriteAccess,
	                          std::ios_base::openmode mode) override;

    private:
        std::string getName() const override;

        void onEachFrame() noexcept override;

    private:
    	struct Instruction
	    {
		    std::function<void()> fnc;
		    std::string filepath;
		    unsigned long id;
		    bool lock; // True if the file is accessed in write mode
		    bool operator==(const Instruction &in)
		    {
		    	return (this->id == in.id);
		    }
	    };

    private:
	    void addInstruction(Instruction &instruction);
    	void instructionPoolFnc();
    private:
		unsigned long _maxInstId = 0;
	    std::atomic_bool _runThread = true;
	    std::mutex _instructionMutex;
	    std::mutex _writeMutex;
	    std::thread _instructionPool; // Async pool
	    std::list<std::string> _writeOpened; // Write opened file path
	    std::list<Instruction> _waitingInstructions;
	    std::condition_variable _threadCv;
    };
}

#endif //CORE_DOLOSRESSOURCESMANAGER_HPP
