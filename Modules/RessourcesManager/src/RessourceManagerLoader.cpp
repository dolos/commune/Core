/*
** EPITECH PROJECT, 2018
** Core
** File  description:
** 
*/

#include <Dolos_SDK/ICore.hpp>
#include "DolosRessourcesManager.hpp"
#include "EventTest.hpp"

static dolos::DolosRessourcesManager *module;

__exported FNC_MODULE_NAME
{
	return (RESSOURCES_MANAGER_NAME);
}

__exported FNC_PRELOAD(core)
{
	module = new dolos::DolosRessourcesManager();
}

__exported FNC_SYS_LOAD(core, setter)
{
	setter->setRessourcesManager(module);
	return (module);
}

__exported FNC_UNLOAD
{
	delete module;
}

__exported FNC_MODULE_TYPE
{
	return MODULE_SYSTEM;
}