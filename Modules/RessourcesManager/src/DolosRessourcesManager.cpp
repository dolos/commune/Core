/*
** EPITECH PROJECT, 2018
** Core
** File  description:
**
*/

#include <climits>
#include <cstdlib>
#include <future>
#include <cstring>
#include <iostream>
#include "DolosRessourcesManager.hpp"
#include <Dolos_SDK/ICore.hpp>

using dolos::DolosRessourcesManager;

dolos::DolosRessourcesManager::DolosRessourcesManager()
: _waitingInstructions(), _instructionPool(&DolosRessourcesManager::instructionPoolFnc, this)
{

}

dolos::DolosRessourcesManager::~DolosRessourcesManager()
{
	_runThread = false;
	_threadCv.notify_one();
	if (_instructionPool.joinable())
		_instructionPool.join();
}

void dolos::DolosRessourcesManager::instructionPoolFnc()
{
	std::list<std::string> writeOpenedCpy;
	std::list<Instruction> waitingInstructionsCpy;
	while (_runThread)
	{
		{
			std::unique_lock<std::mutex> l(_instructionMutex);
			while (_runThread && _waitingInstructions.empty())
				_threadCv.wait(l);
			if (!_runThread)
				return;
			std::unique_lock<std::mutex> l1(_writeMutex);
			if (!_writeOpened.empty())
			    writeOpenedCpy = _writeOpened;
			waitingInstructionsCpy = _waitingInstructions;
		}
		for (const auto &instruction : waitingInstructionsCpy)
		{
			if (instruction.lock)
			{
				bool process = true;
				// Check if file is already opened in write mode
				for (const auto &opened : writeOpenedCpy)
				{
					if (instruction.filepath == opened)
					{
						process = false;
						break;
					}
				}
				// Check if we process the instruction
				if (!process)
					continue;
			}
			// We can process, remove the instruction from the waiting pool
			// and add the file to the write
			{
				std::unique_lock<std::mutex> l(_instructionMutex);
				_waitingInstructions.remove(instruction);
				if (instruction.lock)
				{
					std::unique_lock<std::mutex> l1(_writeMutex);
					_writeOpened.push_back(instruction.filepath);
				}
			}
			instruction.fnc();
		}
	}
}

void dolos::DolosRessourcesManager::addInstruction(DolosRessourcesManager::Instruction &instruction)
{
    std::unique_lock<std::mutex> l(_instructionMutex);
    _waitingInstructions.push_back(std::move(instruction));
	// Notifiy the instruction thread that a new instruction is available
	_threadCv.notify_one();
}

dolos::IRessourcesManager::AccessResult DolosRessourcesManager::accessRead(const std::string &path,
                                                       const OnReadAccess &onReadAccess,
                                                       std::ios_base::openmode mode)
{
	if (!_runThread)
		return NOT_OK;
	std::ifstream stream(path, mode);
	if (!stream.good())
		return (NOT_OK);
	onReadAccess(stream);
	if (stream.is_open())
		stream.close();
	return OK;
}

dolos::IRessourcesManager::AccessResult DolosRessourcesManager::accessWrite(const std::string &path,
                                                        const OnWriteAccess &onWriteAccess,
                                                        std::ios_base::openmode mode)
{
	if (!_runThread)
		return NOT_OK;

	char fullpath_char[RESSOURCES_PATH_MAX_LENGTH];
	std::memset(fullpath_char, 0, RESSOURCES_PATH_MAX_LENGTH);
	realpath(path.c_str(), fullpath_char);
	std::string fullpath(fullpath_char, std::strlen(fullpath_char));
	std::promise<AccessResult> resultPromise;

	// Instruction to be run when access to file is granted
	std::function<void()> fileInstruction = [fullpath, mode, onWriteAccess,
								&resultPromise, this](){
		// Open file, check all is good
		std::ofstream ofstream(fullpath, mode);
		if (!ofstream.good())
		{
			resultPromise.set_value(NOT_OK);
			return;
		}
		// Perform write
		onWriteAccess(ofstream);
		if (ofstream.is_open())
		{
			ofstream.flush();
			ofstream.close();
		}
		std::unique_lock<std::mutex> l(_writeMutex);
		_writeOpened.remove(fullpath);
		resultPromise.set_value(OK);
	};

	// Add write instruction to pool
	Instruction inst = {fileInstruction, fullpath, _maxInstId, true};
	_maxInstId += 1;
	this->addInstruction(inst);
	// Build future and wait for the result
	std::future<AccessResult> resultFuture = resultPromise.get_future();
	return resultFuture.get();
}
void dolos::DolosRessourcesManager::asyncAccessRead(const std::string &path,
                                                    const dolos::IRessourcesManager::OnAsyncReadAccess &onReadAccess,
                                                    std::ios_base::openmode mode)
{
	if (!_runThread)
		return;
	char fullpath_char[RESSOURCES_PATH_MAX_LENGTH];
	std::memset(fullpath_char, 0, RESSOURCES_PATH_MAX_LENGTH);
	realpath(path.c_str(), fullpath_char);
	std::string fullpath(fullpath_char, std::strlen(fullpath_char));

	// Instruction passed to the pool
	std::function<void()> fileInstruction =
		[fullpath, onReadAccess, mode, this]()
		{
			dolos::getCore().getThreadPool().push([fullpath, onReadAccess,
				                                       mode, this](){
				std::ifstream ifstream(fullpath, mode);
				if (!ifstream.good())
				{
					onReadAccess(ifstream, NOT_OK);
					return;
				}
				onReadAccess(ifstream, OK);
				if (ifstream.is_open())
					ifstream.close();
				}, [](){});
		};
	Instruction inst = {fileInstruction, fullpath, _maxInstId, false};
	_maxInstId += 1;
	this->addInstruction(inst);
}

void dolos::DolosRessourcesManager::asyncAccessWrite(const std::string &path,
                                                     const dolos::IRessourcesManager::OnAsyncWriteAccess &onWriteAccess,
                                                     std::ios_base::openmode mode)
{
	if (!_runThread)
		return;
	char fullpath_char[RESSOURCES_PATH_MAX_LENGTH];
	std::memset(fullpath_char, 0, RESSOURCES_PATH_MAX_LENGTH);
	realpath(path.c_str(), fullpath_char);
	std::string fullpath(fullpath_char, std::strlen(fullpath_char));
	std::function<void()> fileInstruction =
		[fullpath, onWriteAccess, mode, this]()
		{
			dolos::getCore().getThreadPool().push([onWriteAccess, mode,
				                                       fullpath, this]{
				std::ofstream ofstream(fullpath, mode);
				if (!ofstream.good())
				{
					onWriteAccess(ofstream, NOT_OK);
					return;
				}
				onWriteAccess(ofstream, OK);
				if (ofstream.is_open())
					ofstream.close();
				std::unique_lock<std::mutex> l(_writeMutex);
				_writeOpened.remove(fullpath);
			}, [](){});
		};
	Instruction inst = {fileInstruction, fullpath, _maxInstId, true};
	_maxInstId += 1;
	this->addInstruction(inst);
}

std::string dolos::DolosRessourcesManager::getName() const {
    return RESSOURCES_MANAGER_NAME;
}

void dolos::DolosRessourcesManager::onEachFrame() noexcept {

}
