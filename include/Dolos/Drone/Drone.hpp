//
// Created by tyker on 2019-11-24.
//

#ifndef CORE_INCLUDE_DOLOS_DRONE_DRONE_HPP
#define CORE_INCLUDE_DOLOS_DRONE_DRONE_HPP

#include <Dolos_SDK/Network/INetworkClient.hpp>

namespace dolos {

class DroneImpl;

struct DroneInfo {
  double lat;
  double lon;
  float  alt;
};

class Drone {
  DroneImpl* drone = nullptr;
  bool Error = false;
  std::shared_ptr<network::INetworkClient> client;
  bool initialized = false, flying = false;
public:
  Drone() = default;
  Drone(const Drone&) = delete;
  Drone& operator=(const Drone&) = delete;
  void Init(std::shared_ptr<network::INetworkClient>);
  void Takeoff(unsigned meters);
  bool HasError() const {
    return Error;
  }
  void Goto(double latitude_deg, double longitude_deg, float altitude_amsl_m, float yaw_deg);
  DroneInfo GetInfo() const;
  void Update();
  void RTL();
  ~Drone();
};

}

#endif //CORE_INCLUDE_DOLOS_DRONE_DRONE_HPP
