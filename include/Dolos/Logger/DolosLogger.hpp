/*
** EPITECH PROJECT, 2018
** spdlog
** File description:
** Logger
*/

#ifndef LOGGER_HPP_
#define LOGGER_HPP_
#define FILENAME "logs/rotating.txt"
#define SIZE_FILE_MAX (1048576 * 5)
#define MAX_FILE_ROTATING 3
#define RIGHTS_REPO 0755
#include <Dolos_SDK/Logger/Ilogger.hpp>
#include <spdlog/spdlog.h>
#include <Dolos/ModuleLoader/SystemLoader.hpp>
#include <string>
#include <queue>
#include <memory>
#include <functional>
#include <iostream>

namespace spd = spdlog;

namespace dolos {

	class DolosLogger : public Ilogger {
	    private:
			DolosLogger();
		public:
			~DolosLogger() = default;
			static Ilogger &Instance();
			void Log(const std::string &message,TYPE_LEVEL level = dolos::TYPE_LEVEL::INFO,TYPE_LOGGING out = dolos::TYPE_LOGGING::STDOUT) override;
		private:
			void Error(const std::string &message, TYPE_LOGGING out = TYPE_LOGGING::STDOUT);
			void Debug(const std::string &message, TYPE_LOGGING out = TYPE_LOGGING::STDOUT);
			void Info(const std::string &message, TYPE_LOGGING out = TYPE_LOGGING::STDOUT);
		protected:
			std::shared_ptr<spdlog::logger> _console;
			//std::shared_ptr<spdlog::logger> _file;
			//dolos::IRessourcesManager *_ressourceMangager;
	};


}
#endif /* !LOGGER_HPP_ */
