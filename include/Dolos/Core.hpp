/*
** EPITECH PROJECT, 2018
** Core
** File  description:
** 
*/

#ifndef CORE_CORE_HPP
#define CORE_CORE_HPP

#include <atomic>
#include <Dolos_SDK/ICore.hpp>
#include <Dolos/ModuleLoader/SystemLoader.hpp>
#include "NetworkManager.hpp"
#include <Dolos/Drone/Drone.hpp>
#include <Dolos/Api/IAPIService.hpp>
#include <memory>

#define API_URL "http://0.0.0.0:30000"
#define PING_TIMELAP 2 // Seconds

namespace dolos
{

	class Core : public ICore
	{
	public:
        Core();
		Core(const Core&) = delete;
		Core(Core&&) = delete;
		Core &operator=(const Core&) = delete;
		~Core() override;

	/**
	 * Those function are only to be used by the Core program itself and
	 * should not be accessed from outside modules
	 */
	public:
	    /**
		 * Initialize the core, load the configuration and the modules
		 * @return True if the core is ready to process
		 */
		bool init();
		/**
		 * Release all the modules, save all data's and stop the core
		 */
		void release();
		/**
		 * Start main loop
		 */
		void loop();
		/**
 		* Get the system loader
 		*/
		SystemLoader &getSystemLoader();

		void stopLooping();

	/**
	 * ICore implementation
	 */
	public:
		IModuleLoader &getModuleLoader() override;
		IRessourcesManager &getRessourcesManager() override;
		IThreadpool &getThreadPool() override;
		Ilogger &getLogger() override;
        network::INetworkManager &getNetworkManager() override;
		dolos::Drone &getDrone() override;

    private:
		bool _registered = false; // Is the drone registered in the API
		std::atomic_bool _run = true;
        std::shared_ptr<network::NetworkManager> _networkManager;
        std::shared_ptr<dolos::Drone> _drone;
		std::unique_ptr<dolos::IAPIService> _apiService;
	};
}

#endif //CORE_CORE_HPP
