/**
 * Author: Mathias Hyrondelle
 * Description: Define API service function
 */
#include <string>
#include <functional>
#ifndef DOLOS_API_SERVICE_HPP
#define DOLOS_API_SERVICE_HPP

namespace dolos {
    template <typename T>
    using Callback = std::function<void(T)>;
    using VoidCallback = std::function<void()>;

    class IAPIService {
        public:
            IAPIService() = default;
            ~IAPIService() = default;
            IAPIService(const IAPIService&) = default;
            IAPIService& operator=(const IAPIService&) = default;

        public:
            /**
             * Initialize the service
             * @param api_url URL to API
             */
            virtual bool initialize(const std::string api_url) = 0;
            /**
             * Release all ressources
             */
            virtual void release() = 0;
            /**
             * Login the drone into the API 
             * @param onResponse Called once we get a response from the API
             * @param onError Called if an error occurs drone-side of the request
            */
            virtual void loginDrone(Callback<bool> onResponse, VoidCallback onError) = 0;
            /**
             * Register the drone as available into the API
             * @param onResponse Called once we get a response
             * @param onError Called if an error occurs drone-side of the request
             */
            virtual void registerAvailable(Callback<bool> onResponse, VoidCallback onError) = 0;
            /**
             * Register the drone as not available into the API
             * @param onResponse Called once we get a response
             * @param onError Called if an error occurs drone-side of the request
             */
            virtual void unregisterAvailable(Callback<bool> onResponse, VoidCallback onError) = 0;
            /**
             * Ping the API 
             */
            virtual void ping() = 0;
    };
}

#endif