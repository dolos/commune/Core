/**
 * Author: Mathias Hyrondelle
 * IAPIService implementation using restclient-cpp library
 */
#include "Dolos/Core.hpp"
#include "IAPIService.hpp"
#include <restclient-cpp/connection.h>
#include <restclient-cpp/restclient.h>
#include <memory>
#include <atomic>

#ifndef API_IMPLEM_REST_CLIENT_CPP
#define API_IMPLEM_REST_CLIENT_CPP

#define LOGIN_ROUTE "/drones/login"
#define AVAILABLE_ROUTE "/drones/register_available"
#define NOT_AVAILABLE_ROUTE "/drones/register_unavailable"
#define PING_ROUTE "/drones/ping"

namespace dolos {
    class RestClientService : public IAPIService {
        private:
            std::string _login_route, _api_url, _token;
            unsigned int _request_count = 0;
            Core &_core;
            std::atomic_bool _ping = false;
        public:
            RestClientService(Core &core);
            ~RestClientService() = default;
            RestClientService(const RestClientService&) = delete;
            RestClientService &operator=(const RestClientService&) = delete;
        public:
            bool initialize(const std::string api_url) override;
            void release() override;
            void loginDrone(Callback<bool> onResponse, VoidCallback onError) override;
            void registerAvailable(Callback<bool> onResponse, VoidCallback onError) override;
            void unregisterAvailable(Callback<bool> onResponse, VoidCallback onError) override;
            void ping() override;
        private:
            RestClient::Connection *getConnection() const;
    };
}

#endif