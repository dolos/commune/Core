/*
** EPITECH PROJECT, 2018
** Core
** File description:
** NetworkManager
*/

#ifndef NETWORKMANAGER_HPP_
#define NETWORKMANAGER_HPP_
#define PRIORITY_NUMBER_ARRAY 6

#include <Dolos_SDK/Network/Network.hpp>
#include <Dolos_SDK/Network/INetworkProvider.hpp>
#include <Dolos_SDK/Network/Network.hpp>
#include <Dolos_SDK/ICore.hpp>
#include <unordered_map>
#include <memory>

namespace dolos
{
    namespace network {
        
        class NetworkManager : public INetworkManager, public ISerializer, public IConnectionHandler, public IPacketHandler {
        public:
            static bool _initialized;
        public:
            NetworkManager();
            NetworkManager(const NetworkManager &) = delete;
            NetworkManager(const NetworkManager &&) = delete;
            NetworkManager(NetworkManager &&) = delete;
            NetworkManager &operator=(const NetworkManager &) = delete;
            virtual ~NetworkManager();

        public:
            // These function are used to register / remove global packets filters
            void addFilter(std::shared_ptr<ISerializer> filter, FilterPriority priority)  override;
            void removeFilter(std::shared_ptr<ISerializer> filter, FilterPriority priority) override;

            /*
             *  These functions are used for deserialization purpose
             *  Devs must register a packet instantiater in order to be able to create it from the serialized string
             */

            void registerInstantiater(const std::string &packetName, PacketInstantiater instantiater) override;


            void onPacketSending(IPacket &packet) override;
            void onPacketReceiving(IPacket &packet) override;
            void onDataSending(std::string &rawData) override;
            void onDataReceived(std::string &rawData) override;

            void onConnection(std::shared_ptr<INetworkClient> client) override;
            void onDisconnection(std::shared_ptr<INetworkClient> client) override;
	  void onPacketReceived(std::shared_ptr<IPacket> packet, std::shared_ptr<INetworkClient> client) override;
            std::shared_ptr<IPacket> instantiate(const std::string &name, const std::string &rawData) override;

        private:
            constexpr static FilterPriority _priorityOrder[PRIORITY_NUMBER_ARRAY] = {
                    FilterPriority::LOWEST,
                    FilterPriority::LOW,
                    FilterPriority::NORMAL,
                    FilterPriority::HIGH,
                    FilterPriority::HIGHEST,
                    FilterPriority ::MONITOR
            };
            std::unordered_multimap<FilterPriority,std::shared_ptr<ISerializer>> _packetFilters;
            std::unordered_map<std::string,PacketInstantiater> _instantiaters;
            int _eh = -1;
        };
    }
}

#endif /* !NETWORKMANAGER_HPP_ */
