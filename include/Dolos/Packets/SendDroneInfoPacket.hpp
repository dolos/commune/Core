//
// Created by tyker on 2019-11-25.
//

#ifndef CORE_INCLUDE_DOLOS_PACKETS_SENDDRONEINFO_HPP
#define CORE_INCLUDE_DOLOS_PACKETS_SENDDRONEINFO_HPP

#include <Dolos/Drone/Drone.hpp>
#include <sstream>

namespace dolos {

class SendDroneInfoPacket : public dolos::network::IPacket {
public:
  DroneInfo data;
  SendDroneInfoPacket() = default;
  SendDroneInfoPacket(const std::string &data);

public:
  std::string getName() const override {
    return "DRONE_INFO";
  }
  std::string serialize() const override {
    std::stringstream ss;
    ss << data.lat << std::endl << data.lon << std::endl << data.alt << std::endl;
    return ss.str();
  }

private:
};
}

#endif //CORE_INCLUDE_DOLOS_PACKETS_SENDDRONEINFO_HPP
