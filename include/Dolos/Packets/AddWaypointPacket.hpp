//
// Created by mathias on 24.04.19.
//

#ifndef CORE_ADDWAYPOINTPACKET_HPP
#define CORE_ADDWAYPOINTPACKET_HPP

#include <Dolos_SDK/Network/Network.hpp>
# define DOLOS_PACKET_ADD_WAYPOINT_NAME "DOLOS_WAYPOINT_PACKET"

namespace dolos {
    class AddWaypointPacket : public dolos::network::IPacket {
    public:
        double lan, lon, alt;
        AddWaypointPacket() = default;
        AddWaypointPacket(const std::string &data);

    public:
        std::string getName() const override;
        std::string serialize() const override;

    private:
    };
}
#endif //CORE_ADDWAYPOINTPACKET_HPP
