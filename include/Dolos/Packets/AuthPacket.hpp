//
// Created by mathias on 29.05.19.
//

#ifndef CORE_AUTHPACKET_HPP
#define CORE_AUTHPACKET_HPP

#include <Dolos_SDK/Network/Network.hpp>
# define DOLOS_PACKET_AUTH_NAME "auth_packet"

namespace dolos {
    class AuthPacket : public dolos::network::IPacket
    {
    public:
        AuthPacket() = default;
        AuthPacket(const std::string &data)
        {

        }

    public:
        std::string getName() const override
        {
            return DOLOS_PACKET_AUTH_NAME;
        }
        std::string serialize() const override
        {
            return "";
        }
    };
}

#endif //CORE_AUTHPACKET_HPP
