/*
** EPITECH PROJECT, 2018
** Core
** File  description:
** 
*/

#ifndef CORE_DOLOSMODULELOADER_HPP
#define CORE_DOLOSMODULELOADER_HPP

#include <queue>
#include <unordered_map>
#include <list>
#include <Dolos_SDK/ModuleLoader/IModuleLoader.hpp>
#include <mutex>

namespace dolos
{	class DolosModuleLoader : public dolos::IModuleLoader
	{
	private:
		struct ModuleAccessor
		{
			IModule *module = nullptr;
			void *handle = nullptr;
			std::string path = nullptr;

			/**
			 * Compare with other accessor
			 * @return
			 */
			bool operator==(const ModuleAccessor&) const;
			/**
			 * Compare with module name
			 * @return
			 */
			bool operator==(const std::string&) const;
		};
		// <name, accessor>
		using ModuleMap = std::unordered_map<std::string, ModuleAccessor>;
	private:
		DolosModuleLoader() = default;

	public:
		DolosModuleLoader(const DolosModuleLoader&) = delete;
		DolosModuleLoader &operator=(const DolosModuleLoader&) = delete;
		~DolosModuleLoader() = default;

	public:
		static DolosModuleLoader &Instance();

		// IModuleLoader implementation
	public:
		IModule *getModule(const char *name) noexcept override;

		// Core implementation
	public:
		void load();
		void release();
        /**
         * Call the onEachFrame function for each modules
         */
        void onEachFrame();

	private:
		/**
		 * Load systems modules
		 */
		void loadSystem();
		/**
		 * Load others modules
		 */
		void loadOthers();
		/**
		 * Release system modules
		 */
		void releaseSystem();
		/**
		 * Release others modules
		 */
		void releaseOthers();
		void unloadModule(ModuleAccessor &module);
		/**
		 * Load all modules in the defined order
		 * @param modules
		 * @param loadOrder
		 */
		void loadModules(ModuleMap &modules,
		                 const std::vector<std::string> &loadOrder);
		/**
		 * Load every system modules in the defined order
		 * @param modules
		 * @param loadOrder
		 */
		void loadSysModules(ModuleMap &modules, const std::vector<std::string> &loadOrder);
		/**
		 * Load the module pointed by ModuleAccessor.handle
		 * @param accessor Accessor to module
		 * @param l Previously loaded modules. Used to check if dependencies
		 * where successfully loaded
		 * @return true if the module was loaded
		 */
		bool load(ModuleAccessor &accessor, const std::vector<std::string> &l);
		/**
		 * Load the system module pointed by ModuleAccessor.handle
		 * @param accessor Accessor to module
		 * @param l Previously loaded modules. Used to check if dependencies
		 * where successfully loaded
		 * @return true if the module was loaded
		 */
		bool loadSys(ModuleAccessor &accessor, const std::vector<std::string> &loaded);
		/**
		 * Preload all modules in the dependencies load order
		 * @param modules pair of string containing module name and module path
		 * @param loadOrder Modules names ordered
		 */
		void preload(const ModuleMap &modules,
		             const std::vector<std::string> &loadOrder);
		/**
		 * Preload the module
		 * @param path Path to the module
		 */
		void preloadModule(const ModuleAccessor &accessor);
		/**
		 * Retrieve all modules
		 * @return map<1, 2> where 1 is module name and 2 is module accessor
		 */
		ModuleMap retrieveModules();
		/**
		 * Retrieve system modules
		 * @return map<1, 2> where 1 is module name and 2 is module accessor
		 */
		ModuleMap retrieveSysModules();
		/**
		 * Find all modules in the folder pointed by path
		 * @param path Folder path
		 * @param append Map to append the modules
		 * @param expectedType The expected module type (system or other)
		 * @return map<1, 2> where 1 is module name and 2 is module path
		 */
		void getModules(const std::string &path,
		                ModuleMap &append, int expectedType);
		/**
		 * Sort module dependencies
		 * @param sorted The already sorted set
		 * @param visited The already visited set
		 * @param modules KeySet of modules <name, path>
		 * @param item Module name
		 */
		void sortDependencies(std::vector<std::string> &sorted,
		                      std::vector<std::string> &visited,
		                      const ModuleMap &modules,
		                      const std::string &item);

		// Wrapper to use in sortDependencies
		std::vector<std::string> getDependencies(const ModuleAccessor &item,
		                                        const ModuleMap &modules);
		/**
		 * Retrieve module dependencies name
		 * @param item ModuleAccessor to access handler
		 * @param modules Vector of loaded modules
		 * @return vector of dependencies name
		 */
		std::vector<std::string> getDependencies(const ModuleAccessor &item,
		                                         const std::vector<std::string> &modules);
	private:
		std::vector<ModuleAccessor> _modules; // Contains all successfully
		// loaded modules
        std::vector<ModuleAccessor> _modulesSystem;
		std::mutex _moduleMutex; // Prevent concurrent access to modules
        std::vector<std::string> _modulesOrder;
		std::vector<std::string> _modulesSysOrder;
	};
}

#endif //CORE_DOLOSMODULELOADER_HPP