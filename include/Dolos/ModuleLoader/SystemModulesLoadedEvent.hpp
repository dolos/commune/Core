//
// Created by mathias on 07.11.18.
//

#ifndef CORE_SYSTEMMODULESLOADEDEVENT_HPP
#define CORE_SYSTEMMODULESLOADEDEVENT_HPP

# define MODULE_SYS_LOAD_EVENT "SysModuleLoaded"

namespace dolos
{
    class SystemModulesLoadedEvent
    {
    public:
        SystemModulesLoadedEvent() = default;
        ~SystemModulesLoadedEvent() = default;
    };
}

#endif //CORE_SYSTEMMODULESLOADEDEVENT_HPP
