/*
** EPITECH PROJECT, 2018
** Core
** File  description:
** 
*/

#ifndef CORE_SYSTEMLOADER_HPP
#define CORE_SYSTEMLOADER_HPP

#include <Dolos_SDK/ModuleLoader/ISystemLoader.hpp>

namespace dolos
{	class SystemLoader : public ISystemLoader {
	private:
		SystemLoader() = default;

	public:
		SystemLoader(const SystemLoader&) = delete;
		SystemLoader &operator=(const SystemLoader&) = delete;
		~SystemLoader() override = default;

	/**
	 * Accessed by Core
	 */
	public:
		static SystemLoader &Instance();
		IRessourcesManager *getRessourcesManager() const;
		network::INetworkProvider *getNetworkServer() const;

		/**
		 * Implementation of ISystemLoader
		 */
	public:
		void setRessourcesManager(IRessourcesManager *module) override;

        void setNetworkServer(network::INetworkProvider *networkServer) override;

    private:
		IRessourcesManager *_ressourcesManager = nullptr;
		network::INetworkProvider *_networkServer = nullptr;
	};
}

#endif //CORE_SYSTEMLOADER_HPP