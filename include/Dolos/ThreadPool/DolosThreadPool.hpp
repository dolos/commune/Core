/*
** EPITECH PROJECT, 2018
** Core
** File  description:
** 
*/

#ifndef CORE_DOLOSTHREADPOOL_HPP
#define CORE_DOLOSTHREADPOOL_HPP

#include <Dolos_SDK/ThreadPool/IThreadPool.hpp>
#include <thread>
#include <list>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <queue>

# define NB_THREADS 3 // Number of threads to run

namespace dolos
{
	class DolosThreadPool : public IThreadpool
	{
	public:
		DolosThreadPool() = default;
		DolosThreadPool(const DolosThreadPool&) = delete;
		DolosThreadPool &operator=(const DolosThreadPool&) = delete;
		~DolosThreadPool() override = default;

	/**
	 * IThreadPool implementation
	 */
	public:
		void push(const Runnable &runnable,
		          const Runnable &onOperationComplete) override;
		void executeOnMainThread(const Runnable &runnable) override;

        void pushDedicated(const Runnable &runnable, const Runnable &onOperationComplete) override;

        /**
         * Used by Core implementation
         */
	public:
		void init();
		void release();
		/**
		 * Pull pending operation for main thread
		 */
		void pullMainQueue();
		static DolosThreadPool &Instance();

	private:
		struct RunnableData
		{
			Runnable runnable = nullptr;
			Runnable onComplete = nullptr;
			RunnableData() = default;
			RunnableData(const Runnable &runnable, const Runnable &onComplete);
			RunnableData(RunnableData&&) noexcept ;
			RunnableData &operator=(RunnableData&&) noexcept ;
			RunnableData(const RunnableData&) = default;
			RunnableData &operator=(const RunnableData&) = default;
			~RunnableData() = default;
		};

	private:
		bool pullRunnable(RunnableData &buffer);

	private:
		std::mutex _runnableMutex;
		std::mutex _mainMutex;
		std::condition_variable _runnableNotifier;
		std::atomic_bool _run = false;
		std::queue<RunnableData> _runnableQueue;
		std::queue<Runnable> _mainQueue;

	private:
		/**
		 * Nested class representing thread
		 */
		class DolosThread
		{
		public:
			DolosThread();
			DolosThread(RunnableData operation);
			DolosThread(const DolosThread&) = delete;
			DolosThread &operator=(const DolosThread&) = delete;
			DolosThread(DolosThread&&) noexcept;
			DolosThread &operator=(DolosThread&&) noexcept;
			~DolosThread();

		public:
			void stop();

		private:
			void threadFnc();
			void threadFnc(RunnableData &operation);

		private:
			std::thread _thread;
			std::atomic_bool _run = true;
		};

	private:
		std::vector<DolosThread> _threads; // Vector of threads
	};
}

#endif //CORE_DOLOSTHREADPOOL_HPP
