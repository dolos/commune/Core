/*
** EPITECH PROJECT, 2018
** Core
** File  description:
** 
*/

#include <sstream>
#include <iostream>
#include <Dolos/Core.hpp>
#include <Dolos_SDK/Network/Network.hpp>
#include <Dolos_SDK/Network/INetworkClient.hpp>
#include <thread>
#include <memory>
#include <mutex>
#include <Dolos/Drone/Drone.hpp>

using dolos::network::PacketReceivedEvent;
using dolos::network::ClientConnectedEvent;
using dolos::network::IPacket;

dolos::Core core;

void log(const std::string &msg)
{
	static std::mutex mutex;
	std::unique_lock<std::mutex> l(mutex);
	std::cout << msg << std::endl;
}

void display(std::string &result)
{
	std::cout << result << std::endl;
}

void waitForInput()
{
	log ("Waiting for input to stop looping");
	std::string buffer;
	std::cin >> buffer;
	std::cin.ignore();
	log("Main: Releasing");
	core.release();
}

int main(int argc, char** argv)
{
	std::cout << "Main: Initializing" << std::endl;
	if (!core.init())
	{
		std::cout << "Main: Failed to initialize Core" << std::endl;
		return (EXIT_FAILURE);
	}
	std::cout << "Main: Initializing end" << std::endl;
	std::thread inputListener(waitForInput);

	log("Main: Looping");
	core.loop();
	if (inputListener.joinable())
		inputListener.join();
	//core.release();
	std::flush(std::cout);
	return (EXIT_SUCCESS);
}