//
// Created by mathias on 04.12.18.
//

#include <gtest/gtest.h>
#include <Dolos/Core.hpp>
#include <thread>
#include <mutex>
#include <condition_variable>
#include "NetworkTest.hpp"

using namespace dolos;
using namespace dolos::network;
using testing::_;
using testing::InSequence;

struct NetworkManagerTest : testing::Test
{
    Core core;

    NetworkManagerTest()
    {
        core.init();
    }

    ~NetworkManagerTest()
    {
        core.release();
    }
};

/**
 * Test simple packet filter with normal priority
 * This test ensure that all filters functions are called by the network manager
 */

TEST_F(NetworkManagerTest, Packet_Filter_Call)
{
    std::shared_ptr<MockSerializer> serializer = std::make_shared<MockSerializer>();
    std::string rawData = "lol";
    NetworkAccessor nm;

    // Setup expectaction, our packet filter functions shall be called once time in this test
    EXPECT_CALL(*serializer, onPacketSending(_)).Times(1);
    EXPECT_CALL(*serializer, onPacketReceiving(_)).Times(1);
    EXPECT_CALL(*serializer, onDataSending(rawData)).Times(1);
    EXPECT_CALL(*serializer, onDataReceived(rawData)).Times(1);

    // Add our filter
    nm.addFilter(serializer, NORMAL);

    // Access the network manager as the ISerializer and advise him of packet receiving / sending
    VirtualPacket p;
    nm.onPacketReceiving(p);
    nm.onPacketSending(p);
    nm.onDataSending(rawData);
    nm.onDataReceived(rawData);
}

/**
 * Test packet filters priority
 * Low filter priority shall be called first
 */
TEST_F(NetworkManagerTest, Packet_Filter_Priorirty)
{
    std::shared_ptr<MockSerializer> serializer = std::make_shared<MockSerializer>(); // LOWEST
    std::shared_ptr<MockSerializer> serializer1 = std::make_shared<MockSerializer>(); // LOW
    std::shared_ptr<MockSerializer> serializer2 = std::make_shared<MockSerializer>(); // NORMAL
    std::shared_ptr<MockSerializer> serializer3 = std::make_shared<MockSerializer>(); // HIGH
    std::shared_ptr<MockSerializer> serializer4 = std::make_shared<MockSerializer>(); // HIGHEST
    std::shared_ptr<MockSerializer> serializer5 = std::make_shared<MockSerializer>(); // MONITOR
    NetworkAccessor nm;
    std::string rawData = "lol";

    // Setup expectaction, our packet filter functions shall be called once time in this test
    {
        InSequence dummy;
        EXPECT_CALL(*serializer, onPacketReceiving(_)).Times(1);
        EXPECT_CALL(*serializer1, onPacketReceiving(_)).Times(1);
        EXPECT_CALL(*serializer2, onPacketReceiving(_)).Times(1);
        EXPECT_CALL(*serializer3, onPacketReceiving(_)).Times(1);
        EXPECT_CALL(*serializer4, onPacketReceiving(_)).Times(1);
        EXPECT_CALL(*serializer5, onPacketReceiving(_)).Times(1);

        EXPECT_CALL(*serializer, onPacketSending(_)).Times(1);
        EXPECT_CALL(*serializer1, onPacketSending(_)).Times(1);
        EXPECT_CALL(*serializer2, onPacketSending(_)).Times(1);
        EXPECT_CALL(*serializer3, onPacketSending(_)).Times(1);
        EXPECT_CALL(*serializer4, onPacketSending(_)).Times(1);
        EXPECT_CALL(*serializer5, onPacketSending(_)).Times(1);

        EXPECT_CALL(*serializer, onDataSending(rawData)).Times(1);
        EXPECT_CALL(*serializer1, onDataSending(rawData)).Times(1);
        EXPECT_CALL(*serializer2, onDataSending(rawData)).Times(1);
        EXPECT_CALL(*serializer3, onDataSending(rawData)).Times(1);
        EXPECT_CALL(*serializer4, onDataSending(rawData)).Times(1);
        EXPECT_CALL(*serializer5, onDataSending(rawData)).Times(1);

        EXPECT_CALL(*serializer, onDataReceived(rawData)).Times(1);
        EXPECT_CALL(*serializer1, onDataReceived(rawData)).Times(1);
        EXPECT_CALL(*serializer2, onDataReceived(rawData)).Times(1);
        EXPECT_CALL(*serializer3, onDataReceived(rawData)).Times(1);
        EXPECT_CALL(*serializer4, onDataReceived(rawData)).Times(1);
        EXPECT_CALL(*serializer5, onDataReceived(rawData)).Times(1);
    }

    // Add our filter
    nm.addFilter(serializer, LOWEST);
    nm.addFilter(serializer1, LOW);
    nm.addFilter(serializer2, NORMAL);
    nm.addFilter(serializer3, HIGH);
    nm.addFilter(serializer4, HIGHEST);
    nm.addFilter(serializer5, MONITOR);


    // Access the network manager as the ISerializer and advise him of packet receiving / sending
    VirtualPacket p;
    nm.onPacketReceiving(p);
    nm.onPacketSending(p);
    nm.onDataSending(rawData);
    nm.onDataReceived(rawData);
}

/**
 * Check if the network manager actually post the event when a connection event happen
 */
TEST_F(NetworkManagerTest, ConnectionHandler_Connect_Event_Handling_Test)
{
    std::mutex mutex;
    std::unique_lock<std::mutex> l(mutex);
    std::condition_variable cv;
    bool success = false;

    // EventHandler on the ClientConnectedEvent
    // If this EH is called, then test is a success;
    core.getEventManager().subscribe<ClientConnectedEvent>(DOLOS_EVENT_CONNECTION, [this, &cv, &success](ClientConnectedEvent &event)
    {
        success = true;
        core.stopLooping();
        cv.notify_one();
        SUCCEED();
    });
    // Async result waiting
    std::thread t([this, &l, &cv, &success](){
        cv.wait_for(l, std::chrono::seconds(2));
        if (success)
            return;
        // FAILED - Release core and print failure reason
        core.stopLooping();
        FAIL();
    });

    // Create and simulate connection of a virtual client on the next drone frame
    core.getThreadPool().executeOnMainThread([this, &cv](){
        try {
            std::shared_ptr<INetworkClient> client = std::make_shared<MockClient>();
            IConnectionHandler &conHandler = dynamic_cast<IConnectionHandler&> (core.getNetworkManager());
            conHandler.onConnection(client);
        } catch (std::exception &e)
        {
            std::cout << "Exception thrown when fetching network manager :" << std::endl;
            std::cout << e.what() << std::endl;
            cv.notify_one(); // Wake up async result
        }
    });
    // Execute the drone loop
    core.loop();
    t.join();
}

/**
 * Check if the network manager trigger the Disconnection Event
 */
TEST_F(NetworkManagerTest, ConnectionHandler_Disconnect_Event_Handling_Test)
{
    std::mutex mutex;
    std::unique_lock<std::mutex> l(mutex);
    std::condition_variable cv;
    bool success = false;

    // EventHandler on the ClientDisconnectedEvent
    // If this EH is called, then test is a success;
    core.getEventManager().subscribe<ClientDisconnectedEvent>(DOLOS_EVENT_DISCONNECTION, [this, &cv, &success](ClientDisconnectedEvent &event)
    {
        success = true;
        core.release();
        cv.notify_one();
        SUCCEED();
    });

    // Async result waiting
    std::thread t([this, &l, &cv, &success](){
        cv.wait_for(l, std::chrono::seconds(2));
        if (success)
            return;
        core.release();
        FAIL();
    });

    // Create and simulate disconnection of a virtual client on the next drone frame
    core.getThreadPool().executeOnMainThread([this, &cv](){
        try {
            std::shared_ptr<INetworkClient> client = std::make_shared<MockClient>();
            IConnectionHandler &conHandler = dynamic_cast<IConnectionHandler&> (core.getNetworkManager());
            conHandler.onDisconnection(client);
        } catch (std::exception &e)
        {
            std::cout << "Exception thrown when fetching network manager :" << std::endl;
            std::cout << e.what() << std::endl;
            cv.notify_one(); // Wake up async result
        }
    });
    // Execute the drone loop
    core.loop();
    t.join();
}

TEST_F(NetworkManagerTest, PacketHandler_Event)
{
    IPacketHandler &handler = dynamic_cast<IPacketHandler&> (core.getNetworkManager());
    std::mutex mutex;
    std::unique_lock<std::mutex> l(mutex);
    std::condition_variable cv;
    bool success = false;

    // EventHandler on the ClientDisconnectedEvent
    // If this EH is called, then test is a success;
    core.getEventManager().subscribe<PacketReceivedEvent>(DOLOS_EVENT_PACKET_RECEIVED, [this, &cv, &success](PacketReceivedEvent &event)
    {
        success = true;
        core.release();
        cv.notify_one();
        SUCCEED();
    });

    // Async result waiting
    std::thread t([this, &l, &cv, &success](){
        cv.wait_for(l, std::chrono::seconds(2));
        if (success)
            return;
        core.release();
        FAIL();
    });

    handler.onPacketReceived(std::make_shared<VirtualPacket>(), std::make_shared<MockClient>());
    t.join();
}

/**
 * This test ensure that packet are correctly instantiated
 */
TEST_F(NetworkManagerTest, PacketHandler_Instantiate)
{
    IPacketHandler &handler = dynamic_cast<IPacketHandler&> (core.getNetworkManager());
    core.getNetworkManager().registerInstantiater(VIRTUAL_PACKET, [](const std::string &rawData){
       return (std::make_shared<VirtualPacket>());
    });

    auto ptr = handler.instantiate(VIRTUAL_PACKET, "unused");
    ASSERT_FALSE(ptr == nullptr);
    ASSERT_EQ(ptr->getName(), VIRTUAL_PACKET);
}
