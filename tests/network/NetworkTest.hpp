//
// Created by mathias on 04.12.18.
//

#ifndef CORE_NETWORKTEST_HPP
#define CORE_NETWORKTEST_HPP

#include <gmock/gmock.h>
#include <Dolos_SDK/Network/Network.hpp>
#include <Dolos_SDK/Network/INetworkClient.hpp>
#include <Dolos/NetworkManager.hpp>

# define VIRTUAL_PACKET "VirtualPacket"

class VirtualPacket : public dolos::network::IPacket
{
public:
    std::string getName() const override {
        return VIRTUAL_PACKET;
    }

    std::string serialize() const override {
        return "SerializedData";
    }
};

class MockSerializer : public dolos::network::ISerializer
{
public:
    MOCK_METHOD1(onPacketSending, void(dolos::network::IPacket&));
    MOCK_METHOD1(onPacketReceiving, void(dolos::network::IPacket&));
    MOCK_METHOD1(onDataSending, void(std::string&));
    MOCK_METHOD1(onDataReceived, void(std::string&));
};

class MockClient : public dolos::network::INetworkClient
{
public:
    MOCK_METHOD1(send, void(dolos::network::IPacket&));
    MOCK_METHOD0(close, void());
    MOCK_METHOD0(isOpen, bool());
    MOCK_METHOD0(getProtocol, dolos::network::Protocol());
};

class NetworkAccessor : public dolos::network::NetworkManager
{
public:
    NetworkAccessor() = default;
    ~NetworkAccessor() = default;
};

#endif //CORE_NETWORKTEST_HPP
