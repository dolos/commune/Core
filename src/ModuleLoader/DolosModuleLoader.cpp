/*
** EPITECH PROJECT, 2018
** Core
** File  description:
** 
*/

#include <dlfcn.h>
#include <iostream>
#include <experimental/filesystem>
#include "Dolos/ModuleLoader/DolosModuleLoader.hpp"
#include <Dolos/Core.hpp>
#include <Dolos/ModuleLoader/SystemModulesLoadedEvent.hpp>


# define OPEN_FLAG (RTLD_LAZY | RTLD_GLOBAL)

// ModuleAccessor
bool dolos::DolosModuleLoader::ModuleAccessor::operator==(const
                                                          DolosModuleLoader::ModuleAccessor &a) const
{
	return (a.handle == handle);
}
bool dolos::DolosModuleLoader::ModuleAccessor::operator==(const std::string
                                                          &module) const
{
	return this->module && this->module->getName() == module;
}

// ModuleLoader
typedef void (*ModulePreload)(dolos::ICore*);
typedef dolos::IModule* (*ModuleLoad) (dolos::ICore*);
typedef dolos::IModule* (*ModuleSysLoad) (dolos::ICore*, dolos::ISystemLoader*);
typedef void (*ModuleUnload)();
typedef const char** (*ModuleDependencies) ();
typedef int (*ModuleDependenciesSize)();
typedef const char* (*ModuleName)();
typedef int (*ModuleType)();
namespace fs = std::experimental::filesystem;
using dolos::DolosModuleLoader;

dolos::IModule *dolos::DolosModuleLoader::getModule(const char *name) noexcept
{
	std::unique_lock<std::mutex> l(_moduleMutex);
	auto it = std::find(_modules.begin(), _modules.end(), name);
	if (it == _modules.end())
		return (nullptr);
	return (it->module);
}

void dolos::DolosModuleLoader::load()
{
	loadSystem();
	dolos::SystemModulesLoadedEvent event;
	getCore().getEventManager().post(MODULE_SYS_LOAD_EVENT, event);
	loadOthers();
}

void dolos::DolosModuleLoader::loadOthers()
{
	std::vector<std::string> visited;
	ModuleMap modules = retrieveModules();
	std::for_each(modules.cbegin(), modules.cend(),
	              [this, modules, &visited](const std::pair<std::string, ModuleAccessor> &p)
	              {
		              sortDependencies(_modulesOrder, visited, modules, p.first);
	              });
	preload(modules, _modulesOrder);
	loadModules(modules, _modulesOrder);
}

void dolos::DolosModuleLoader::loadSystem()
{
	std::vector<std::string> visited;
	ModuleMap modules = retrieveSysModules();

	std::for_each(modules.cbegin(), modules.cend(),
	[this, modules, &visited](const std::pair<std::string, ModuleAccessor> &p)
	{
		  sortDependencies(_modulesSysOrder, visited, modules, p.first);
	});
//	preload(modules, _modulesSysOrder);
	loadSysModules(modules, _modulesSysOrder);
}

void dolos::DolosModuleLoader::release()
{
	releaseOthers();
	releaseSystem();
}

void dolos::DolosModuleLoader::releaseOthers()
{
	std::vector<ModuleAccessor> modulesCpy;
	{
		std::unique_lock<std::mutex> l(_moduleMutex);
		modulesCpy = _modules;
	}
	std::for_each(modulesCpy.rbegin(), modulesCpy.rend(),
	              [this, &modulesCpy](ModuleAccessor &access)
	              {
		              unloadModule(access);
	              });
}

void dolos::DolosModuleLoader::releaseSystem()
{
	std::vector<ModuleAccessor> modulesCpy;
	{
		std::unique_lock<std::mutex> l(_moduleMutex);
		modulesCpy = _modulesSystem;
	}
	std::for_each(modulesCpy.rbegin(), modulesCpy.rend(),
	[this, &modulesCpy](ModuleAccessor &access)
	{
		  unloadModule(access);
	});
}

void dolos::DolosModuleLoader::unloadModule(DolosModuleLoader::ModuleAccessor &module)
{
	std::cout << "Unloading " << module.path << std::endl;
	dlerror();
	auto unloadFnc = (ModuleUnload) dlsym(module.handle,
	                                              FNC_UNLOAD_STR);
	if (dlerror())
		std::cerr << "Warning: " + module.path + " has no unload symbol";
	else
		unloadFnc();
	std::unique_lock<std::mutex> l(_moduleMutex);
	dlclose(module.handle);
	auto it = std::find(_modules.begin(), _modules.end(), module);
	if (it != _modules.end())
		_modules.erase(it);
}

void dolos::DolosModuleLoader::loadModules(DolosModuleLoader::ModuleMap &modules,
                                           const std::vector<std::string> &loadOrder)
{
	std::vector<std::string> loaded;
	std::for_each(loadOrder.cbegin(), loadOrder.cend(),
	[this, &modules, &loaded](const std::string &m)
	{
		auto accessor = modules.find(m);
		if (accessor != modules.end())
		{
			if (load(accessor->second, loaded))
				loaded.push_back(m);
		}
	});
}

void dolos::DolosModuleLoader::loadSysModules(DolosModuleLoader::ModuleMap &modules,
										   const std::vector<std::string> &loadOrder)
{
	std::vector<std::string> loaded;
	std::for_each(loadOrder.cbegin(), loadOrder.cend(),
	[this, &modules, &loaded](const std::string &m)
	{
		auto accessor = modules.find(m);
		if (accessor != modules.end())
		{
			if (loadSys(accessor->second, loaded))
				loaded.push_back(m);
		}
	});
}

bool dolos::DolosModuleLoader::loadSys(DolosModuleLoader::ModuleAccessor &accessor,
									const std::vector<std::string> &loaded)
{
	std::cout << "Loading " << accessor.path << std::endl;
	dlerror();
	bool ret = true;
	std::vector<std::string> dependencies = getDependencies(accessor, loaded);
	std::for_each(dependencies.cbegin(), dependencies.cend(),
	[loaded, &ret, accessor](const std::string &d){
		if (std::find(loaded.cbegin(), loaded.cend(), d) == loaded.cend())
		{
			ret = false;
			std::cerr << "Can't load system module " << accessor.path
			<< ": dependency " << d << " missing" << std::endl;
		}
	});
	if (!ret)
		return (ret);
	auto loadFnc = (ModuleSysLoad) dlsym(accessor.handle, FNC_SYS_LOAD_STR);
	if (dlerror())
	{
		std::cerr << "Unable to load system module " << accessor.path
				  << ": Load function not found" << std::endl;
		return (false);
	}
	dolos::Core &core = dynamic_cast<dolos::Core&>(dolos::getCore());
	accessor.module = loadFnc(&core, &(core.getSystemLoader()));
	std::unique_lock<std::mutex> l(_moduleMutex);
	_modulesSystem.push_back(accessor);
	return (true);
}

bool dolos::DolosModuleLoader::load(DolosModuleLoader::ModuleAccessor &accessor,
                                    const std::vector<std::string> &loaded)
{
	std::cout << "Loading " << accessor.path << std::endl;
	dlerror();
	bool ret = true;
	std::vector<std::string> dependencies = getDependencies(accessor, loaded);
	std::for_each(dependencies.cbegin(), dependencies.cend(),
	[loaded, &ret, accessor](const std::string &d){
		if (std::find(loaded.cbegin(), loaded.cend(), d) == loaded.cend())
		{
			ret = false;
			std::cerr << "Can't load module " << accessor.path
			          << ": dependency " << d << " missing" << std::endl;
		}
	});
	if (!ret)
		return (ret);
	auto loadFnc = (ModuleLoad) dlsym(accessor.handle, FNC_LOAD_STR);
	if (dlerror())
	{
		std::cerr << "Unable to load module " << accessor.path
		          << ": Load function not found" << std::endl;
		return (false);
	}
	if (!(accessor.module = loadFnc(&dolos::getCore())))
		throw std::runtime_error("Can't load " + accessor.path + ": module is"
														   " null");
	std::unique_lock<std::mutex> l(_moduleMutex);
	_modules.push_back(accessor);
	return (true);
}

DolosModuleLoader::ModuleMap dolos::DolosModuleLoader::retrieveModules()
{
	std::string moduleFolder = "modules";
	ModuleMap modules;
	fs::create_directories(moduleFolder);
	getModules(moduleFolder, modules, MODULE_OTHER);
	return (modules);
}

DolosModuleLoader::ModuleMap dolos::DolosModuleLoader::retrieveSysModules()
{
	std::string moduleSystemFolder = "modules/system";
	ModuleMap modules;
	fs::create_directories(moduleSystemFolder);
	getModules(moduleSystemFolder, modules, MODULE_SYSTEM);
	return (modules);
}

void dolos::DolosModuleLoader::preload(const DolosModuleLoader::ModuleMap &modules,
                                       const std::vector<std::string> &loadOrder)
{
	std::for_each(loadOrder.cbegin(), loadOrder.cend(),
	              [this, modules](const std::string &moduleName)
	              {
		              auto it = modules.find(moduleName);
		              if (it == modules.cend()) // Should not happen
			              return;
		              preloadModule(it->second);
	              });
}
void dolos::DolosModuleLoader::preloadModule(const DolosModuleLoader::ModuleAccessor &accessor)
{
	dlerror();
	auto preloadFnc = (ModulePreload) dlsym(accessor.handle, FNC_PRELOAD_STR);
	if (dlerror())
	{
		dlclose(accessor.handle);
		throw std::runtime_error("Unable to preload "
						   + accessor.path + ": no preload symbol found");
	}
	preloadFnc(&dolos::getCore());
}

void dolos::DolosModuleLoader::getModules(const std::string &path,
                                          DolosModuleLoader::ModuleMap &append,
                                          int expectedType)
{
	dlerror();
	std::string filepath;

	for (const auto &item : fs::directory_iterator(path))
	{
		if (fs::is_directory(item))
			continue;
		filepath = item.path();
		void *handle = dlopen(filepath.c_str(), OPEN_FLAG);
		if (handle == nullptr)
			throw std::runtime_error("Unable to load module " + filepath + ", reason:\n"
				                         + std::string(dlerror()));
		dlerror();
		auto moduleTypeFnc = (ModuleType) dlsym(handle, FNC_MODULE_TYPE_STR);
		if (dlerror())
		{
			dlclose(handle);
			throw std::runtime_error(
				"Unable to load module " + filepath + ", module type not "
										  "specified");
		}
		if (moduleTypeFnc() != expectedType)
		{
			dlclose(handle);
			throw std::runtime_error(
				"Unable to load module " + filepath + ", module in wrong "
										  "directory, try to move it to "
			"modules/ or modules/system");
		}
		auto moduleNameFnc = (ModuleName) dlsym(handle, FNC_MODULE_NAME_STR);
		if (dlerror())
		{
			dlclose(handle);
			throw std::runtime_error(
				"Unable to load module " + filepath + ", no "
				                                      "name specified");
		}
		ModuleAccessor accessor = {nullptr, handle, filepath};
		append.emplace(moduleNameFnc(), accessor);
	}
}

void dolos::DolosModuleLoader::sortDependencies(std::vector<std::string> &sorted,
                                                std::vector<std::string> &visited,
                                                const DolosModuleLoader::ModuleMap &modules,
                                                const std::string &item)
{
	// Item has not been visited
	if (std::find(visited.cbegin(), visited.cend(), item) == visited.cend())
	{
		visited.push_back(item);
		auto dependencies = getDependencies(modules.find(item)->second, modules);
		std::for_each(dependencies.cbegin(), dependencies.cend(),
		              [this, &sorted, &visited, modules](const std::string &dep)
		              {
			              sortDependencies(sorted, visited, modules, dep);
		              });
		sorted.push_back(item);
	}
		// Item has been visited but not sorted
	else if (std::find(sorted.cbegin(), sorted.cend(), item) == sorted.cend())
		throw std::runtime_error("Cyclic dependencies");
}

std::vector<std::string> dolos::DolosModuleLoader::getDependencies(const DolosModuleLoader::ModuleAccessor &item,
                                                                   const DolosModuleLoader::ModuleMap &modules)
{
	std::vector<std::string> loaded;
	std::for_each(modules.cbegin(), modules.cend(),
	[&loaded](const std::pair<std::string, ModuleAccessor> &it)
	{
		loaded.push_back(it.first);
	});
	return getDependencies(item, loaded);
}

std::vector<std::string> dolos::DolosModuleLoader::getDependencies(const DolosModuleLoader::ModuleAccessor &item,
                                                                   const std::vector<std::string> &modules)
{
	dlerror();
	std::vector<std::string> dependencies;
	auto dependenciesFnc = (ModuleDependencies) dlsym(item.handle,
	                                                  FNC_MODULE_DEPENDENCY_STR);
	if (dlerror()) // No dependencies function, assuming there are none
		return (dependencies);
	auto dependenciesSize = (ModuleDependenciesSize) dlsym(item.handle,
	                                                       FNC_MODULE_DEPENDENCY_SIZE_STR);
	if (dlerror())
		throw std::logic_error("Dependencies set but no dependencies size "
		                       "symbol found");
	auto fromLib = dependenciesFnc();
	auto size = dependenciesSize();
	std::string name;
	for (int i = 0; i < size; i++)
	{
		name = fromLib[i];
		if (std::find(modules.cbegin(), modules.cend(), name) == modules.cend())
			throw std::runtime_error("Module " + item.path + " depends on "
			                                                 "module " + name + ", "
			                                                                    "wich can't be found");
		dependencies.push_back(name);
	}
	return (dependencies);
}
DolosModuleLoader &dolos::DolosModuleLoader::Instance()
{
	static DolosModuleLoader loader;
	return (loader);
}

void dolos::DolosModuleLoader::onEachFrame() {
    for (auto accessor : _modulesSystem)
        accessor.module->onEachFrame();
    for (auto accessor : _modules)
        accessor.module->onEachFrame();
}
