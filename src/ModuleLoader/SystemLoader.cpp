/*
** EPITECH PROJECT, 2018
** Core
** File  description:
** 
*/

#include <Dolos/ModuleLoader/SystemLoader.hpp>

dolos::SystemLoader &dolos::SystemLoader::Instance()
{
	static SystemLoader loader;
	return (loader);
}

void dolos::SystemLoader::setRessourcesManager(IRessourcesManager *module)
{
	_ressourcesManager = module;
}
dolos::IRessourcesManager *dolos::SystemLoader::getRessourcesManager() const
{
	return _ressourcesManager;
}

void dolos::SystemLoader::setNetworkServer(dolos::network::INetworkProvider *networkServer) {
    _networkServer = networkServer;
}

dolos::network::INetworkProvider *dolos::SystemLoader::getNetworkServer() const {
    return _networkServer;
}
