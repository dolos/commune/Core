//
// Created by jean-david on 11/9/18.
//

#include <Dolos/ModuleLoader/SystemModulesLoadedEvent.hpp>
#include <Dolos/NetworkManager.hpp>
#include <Dolos/Core.hpp>
#include <iostream>
#include <signal.h>

using dolos::SystemModulesLoadedEvent;

void dolos::network::NetworkManager::onPacketSending(IPacket &packet)
{
    try
    {
        for (int index = 0; index < PRIORITY_NUMBER_ARRAY; index++)
        {
            for (const auto &it :  _packetFilters)
            {
                if (it.first == _priorityOrder[index])
                    it.second->onPacketSending(packet);
            }
        }
    }
    catch (const std::exception& e)
    {
        throw std::runtime_error("Logger not implemented yet");
        //TODO: LOGGER IMPLEMENTATION
    }
}

void dolos::network::NetworkManager::onPacketReceiving(IPacket &packet)
{
    try
    {
        for (int index = 0; index < PRIORITY_NUMBER_ARRAY; index++)
        {
            for (const auto &it :  _packetFilters)
            {
                if (it.first == _priorityOrder[index])
                    it.second->onPacketReceiving(packet);
            }
        }
    }
    catch (const std::exception& e)
    {
        throw std::runtime_error("Logger not implemented yet");
        //TODO: LOGGER IMPLEMENTATION
    }
}

void dolos::network::NetworkManager::onDataSending(std::string &rawData)
{
    try
    {
        for (int index = 0; index < PRIORITY_NUMBER_ARRAY; index++)
        {
            for (const auto &it :  _packetFilters)
            {
                if (it.first == _priorityOrder[index])
                    it.second->onDataSending(rawData);
            }
        }
    }
    catch (const std::exception& e)
    {
        throw std::runtime_error("Logger not implemented yet");
        //TODO: LOGGER IMPLEMENTATION
    }
}

void dolos::network::NetworkManager::onDataReceived(std::string &rawData)
{
    try
    {
        for (int index = 0; index < PRIORITY_NUMBER_ARRAY; index++)
        {
            for (const auto &it :  _packetFilters)
            {
                if (it.first == _priorityOrder[index])
                    it.second->onDataReceived(rawData);
            }
        }
    }
    catch (const std::exception& e)
    {
        throw std::runtime_error("Logger not implemented yet");
        //TODO: LOGGER IMPLEMENTATION
    }
}

void dolos::network::NetworkManager::onConnection(std::shared_ptr<INetworkClient> client)
{
    dolos::network::ClientConnectedEvent event(client);
    dolos::getCore().getEventManager().post<ClientConnectedEvent>(DOLOS_EVENT_CONNECTION, event);
}

void dolos::network::NetworkManager::onDisconnection(std::shared_ptr<INetworkClient> client)
{
    dolos::network::ClientDisconnectedEvent event(client);
    dolos::getCore().getEventManager().post<ClientDisconnectedEvent>(DOLOS_EVENT_DISCONNECTION, event);
}

void dolos::network::NetworkManager::addFilter(std::shared_ptr<ISerializer> filter, FilterPriority priority)
{
    _packetFilters.emplace(priority, filter);
}

void dolos::network::NetworkManager::removeFilter(std::shared_ptr<ISerializer> filter, FilterPriority priority) {
    using MMAPIterator = std::unordered_multimap<FilterPriority, std::shared_ptr<ISerializer>>::iterator;

    MMAPIterator removeIt;
    for (auto it = _packetFilters.begin(); it != _packetFilters.end(); ++it)
    {
        if (it->first == priority && it->second == filter)
        {
            removeIt = it;
            break;
        }
    }
    if (removeIt != _packetFilters.end())
        _packetFilters.erase(removeIt);
}

std::shared_ptr<dolos::network::IPacket> dolos::network::NetworkManager::instantiate(const std::string &name, const std::string &rawData)
{
    auto it = _instantiaters.find(name);
    if (it == _instantiaters.end())
    {
        std::cerr << "No packet instantiater registered for packet name " << name << std::endl;
        return nullptr;
    }
    return _instantiaters.find(name)->second(rawData);
}


void dolos::network::NetworkManager::registerInstantiater(const std::string &packetName, PacketInstantiater instantiater)
{
    _instantiaters.emplace(packetName, instantiater);
}

void dolos::network::NetworkManager::onPacketReceived(std::shared_ptr<IPacket> packet, std::shared_ptr<INetworkClient> client)
{
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  PacketReceivedEvent event(packet, client);
    dolos::getCore().getEventManager().post<PacketReceivedEvent>(DOLOS_EVENT_PACKET_RECEIVED, event);
}

dolos::network::INetworkProvider *g_network;
bool dolos::network::NetworkManager::_initialized = false;

dolos::network::NetworkManager::NetworkManager() {
    _initialized = false;
    _eh = dolos::getCore().getEventManager().subscribe<SystemModulesLoadedEvent>(MODULE_SYS_LOAD_EVENT, [this](SystemModulesLoadedEvent &event){
        dolos::Core &core = dynamic_cast<dolos::Core&>(dolos::getCore());
        dolos::network::INetworkProvider *provider = core.getSystemLoader().getNetworkServer();
        if (provider == nullptr)
            return;
        provider->setConnectionHandler(dynamic_cast<dolos::network::IConnectionHandler*>(this));
        provider->setPacketHandler(dynamic_cast<dolos::network::IPacketHandler*>(this));
        provider->setSerializer(dynamic_cast<dolos::network::ISerializer*>(this));
        std::cout << "Opening server on TCP:2544" << std::endl;
        _initialized = provider->open(2544, dolos::network::TCP);
        g_network = provider;
        signal(SIGTERM, [](int) {
            if (!_initialized)
            std::cout << "Closing server on TCP:2544" << std::endl;
            g_network->close(2544);
        });
    });
}

dolos::network::NetworkManager::~NetworkManager() {
    if (_eh != -1)
        dolos::getCore().getEventManager().unsubscribe<SystemModulesLoadedEvent>(MODULE_SYS_LOAD_EVENT, _eh);
}
