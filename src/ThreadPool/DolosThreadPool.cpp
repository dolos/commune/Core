/*
** EPITECH PROJECT, 2018
** Core
** File  description:
** 
*/

#include <Dolos/ThreadPool/DolosThreadPool.hpp>
#include <iostream>

dolos::DolosThreadPool &dolos::DolosThreadPool::Instance()
{
	static DolosThreadPool instance;
	return (instance);
}

void dolos::DolosThreadPool::push(const dolos::IThreadpool::Runnable &runnable,
                                  const dolos::IThreadpool::Runnable &onOperationComplete)
{
	RunnableData runnableData(runnable, onOperationComplete);
	{
		std::unique_lock<std::mutex> l(_runnableMutex);
		_runnableQueue.push(runnableData);
	}
	_runnableNotifier.notify_one();
}

void dolos::DolosThreadPool::executeOnMainThread(const dolos::IThreadpool::Runnable &runnable)
{
	std::unique_lock<std::mutex> l(_mainMutex);
	_mainQueue.push(runnable);
}

void dolos::DolosThreadPool::init()
{
	_run = true;
	_threads = std::vector<DolosThread>(NB_THREADS);
}

void dolos::DolosThreadPool::release()
{
	_run = false;
	_runnableNotifier.notify_all();
	// Stop all threads
	std::for_each(_threads.begin(), _threads.end(),
	              [](DolosThread &thread) { thread.stop(); });
}

void dolos::DolosThreadPool::pullMainQueue()
{
	std::queue<Runnable> queue;
	{
		std::unique_lock<std::mutex> l(_mainMutex);
		queue = std::move(_mainQueue);
		_mainQueue = std::queue<Runnable>();
	}
	while (!queue.empty())
	{
		Runnable &runnable = queue.front();
		runnable();
		queue.pop();
	}
}

bool dolos::DolosThreadPool::pullRunnable(RunnableData &buffer)
{
	std::unique_lock<std::mutex> l(_runnableMutex);
	while (_run && _runnableQueue.empty()) // Wait until a task is available
		// or the thread pool is ending
		_runnableNotifier.wait(l);
	if (!_run && _runnableQueue.empty()) // If queue is not empty, we must
		// clear it before stopping threads
		return (false);
	RunnableData &d = _runnableQueue.front();
	buffer.runnable = d.runnable;
	buffer.onComplete = d.onComplete;
	_runnableQueue.pop();
	return (true);
}

void dolos::DolosThreadPool::pushDedicated(const dolos::IThreadpool::Runnable &runnable,
                                           const dolos::IThreadpool::Runnable &onOperationComplete) {
    RunnableData data;
    data.runnable = runnable;
    data.onComplete = onOperationComplete;
    _threads.emplace_back(data);
}

// Thread

dolos::DolosThreadPool::DolosThread::DolosThread()
{
    _thread = std::thread([this](){
        threadFnc();
    });
}

dolos::DolosThreadPool::DolosThread::DolosThread(dolos::DolosThreadPool::RunnableData operation)
{
    _thread = std::thread([this, &operation](){
        threadFnc(operation);
    });
}

dolos::DolosThreadPool::DolosThread::DolosThread
	(dolos::DolosThreadPool::DolosThread &&dolosThread) noexcept
{
	_thread = std::move(dolosThread._thread);
	_run = (dolosThread._run) ? true : false;
}

dolos::DolosThreadPool::DolosThread &dolos::DolosThreadPool::DolosThread::operator=(
	dolos::DolosThreadPool::DolosThread &&dolosThread) noexcept
{
	_thread = std::move(dolosThread._thread);
	_run = (dolosThread._run) ? true : false;
	return *this;
}

dolos::DolosThreadPool::DolosThread::~DolosThread()
{
	this->stop();
}

void dolos::DolosThreadPool::DolosThread::stop()
{
	if (_thread.joinable())
		_thread.join();
}

void dolos::DolosThreadPool::DolosThread::threadFnc()
{
	DolosThreadPool &threadPool = DolosThreadPool::Instance();
	RunnableData buffer;
	bool loop = true;
	while (loop)
	{
		if ((loop = threadPool.pullRunnable(buffer)))
		{
			buffer.runnable();
			threadPool.executeOnMainThread(buffer.onComplete);
		}
		buffer.onComplete = nullptr;
		buffer.runnable = nullptr;
	}
	_run = false;
}

void dolos::DolosThreadPool::DolosThread::threadFnc(dolos::DolosThreadPool::RunnableData &operation) {
    operation.runnable();
    DolosThreadPool::Instance().executeOnMainThread(operation.onComplete);
}

// Runnable data

dolos::DolosThreadPool::RunnableData::RunnableData
	(dolos::DolosThreadPool::RunnableData &&data) noexcept
{
	runnable = std::move(data.runnable);
	onComplete = std::move(data.onComplete);
}
dolos::DolosThreadPool::RunnableData &dolos::DolosThreadPool::RunnableData::operator=(
	dolos::DolosThreadPool::RunnableData &&data) noexcept
{
	runnable = std::move(data.runnable);
	onComplete = std::move(data.onComplete);
	return *this;
}

dolos::DolosThreadPool::RunnableData::RunnableData
	(const dolos::IThreadpool::Runnable &runnable,
	 const dolos::IThreadpool::Runnable &onComplete)
{
	this->runnable = runnable;
	this->onComplete = onComplete;
}
