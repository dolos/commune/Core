//
// Created by tyker on 2019-11-24.
//

#include <mavsdk/mavsdk.h>
#include <mavsdk/plugins/action/action.h>
#include <mavsdk/plugins/mission/mission.h>
#include <mavsdk/plugins/telemetry/telemetry.h>

#include <functional>
#include <future>
#include <iostream>
#include <memory>
#include "Dolos/Drone/Drone.hpp"
#include "Dolos/Packets/SendDroneInfoPacket.hpp"

#define ERROR_CONSOLE_TEXT "\033[31m"
#define TELEMETRY_CONSOLE_TEXT "\033[34m"
#define NORMAL_CONSOLE_TEXT "\033[0m"

namespace dolos {

using namespace mavsdk;
using namespace std::chrono;
using namespace std::this_thread;

struct DroneImpl {
  Mavsdk dc;
  System &system;
  std::shared_ptr<Action> action;
  std::shared_ptr<Telemetry> telemetry;
  DroneImpl() : dc(), system(dc.system()) {}
};

static void handle_action_err_exit(Action::Result result, const std::string& message);
static void handle_mission_err_exit(Mission::Result result, const std::string& message);
static void handle_connection_err_exit(ConnectionResult result, const std::string& message);

void Drone::Init(std::shared_ptr<network::INetworkClient> c) {
  client = std::move(c);
  if (initialized) return;
  initialized = true;
  drone = new DroneImpl();
  std::string connection_url;
  ConnectionResult connection_result;

  connection_url = "udp://:14540";

  std::cout << "Connection URL: " << connection_url << std::endl;

  {
    auto prom = std::make_shared<std::promise<void>>();
    auto future_result = prom->get_future();

    std::cout << "Waiting to discover system..." << std::endl;
    drone->dc.register_on_discover([prom](uint64_t uuid) {
      std::cout << "Discovered system with UUID: " << uuid << std::endl;
      prom->set_value();
    });

    connection_result = drone->dc.add_any_connection(connection_url);
    handle_connection_err_exit(connection_result, "Connection failed: ");

    future_result.get();
  }

  drone->dc.register_on_timeout([](uint64_t uuid) {
    std::cout << "System with UUID timed out: " << uuid << std::endl;
    std::cout << "Exiting." << std::endl;
    exit(0);
  });

  System& system = drone->dc.system();
  drone->action = std::make_shared<Action>(system);
  drone->telemetry = std::make_shared<Telemetry>(system);

  while (!drone->telemetry->health_all_ok()) { // attends que px4 et le model de drone soit pret à etre desarm
    std::cout << "Waiting for system to be ready" << std::endl;
    sleep_for(seconds(1));
  }
  std::cout << "System ready" << std::endl;

}

void Drone::Update() {
  if (client) {
    SendDroneInfoPacket pack;
    pack.data = GetInfo();
    client->send(pack);
  }
}

Drone::~Drone() {
  delete drone;
}

void Drone::Takeoff(unsigned meters) {
  if (drone->telemetry->armed()) return;
  if (flying) return;
  flying = true;
  // Arm vehicle
  std::cout << "Arming..." << std::endl;
  const Action::Result arm_result = drone->action->arm();

  if (arm_result != Action::Result::SUCCESS) {
    std::cout << ERROR_CONSOLE_TEXT << "Arming failed:" << Action::result_str(arm_result)
              << NORMAL_CONSOLE_TEXT << std::endl;
    Error = true;
    flying = false;
    return;
  }

  // Take off
  std::cout << "Taking off..." << std::endl;
  const Action::Result takeoff_result = drone->action->takeoff();
  if (takeoff_result != Action::Result::SUCCESS) {
    std::cout << ERROR_CONSOLE_TEXT << "Takeoff failed:" << Action::result_str(takeoff_result)
              << NORMAL_CONSOLE_TEXT << std::endl;
    Error = true;
    flying = false;
    return;
  }

}

void Drone::Goto(double latitude_deg, double longitude_deg, float altitude_amsl_m, float yaw_deg) {
  std::cout << "Goto : lat = " << latitude_deg << " lon = " << longitude_deg << " alt = " << altitude_amsl_m << " yaw = " << yaw_deg << std::endl;
  drone->action->goto_location(latitude_deg, longitude_deg, altitude_amsl_m, yaw_deg);
}

void Drone::RTL() {
  if (!flying) return;
  // RTL = revenir sur la position de départ
  std::cout << "Commanding RTL..." << std::endl;
  const Action::Result result = drone->action->return_to_launch();
  if (result != Action::Result::SUCCESS)
    std::cout << "Failed to command RTL (" << Action::result_str(result) << ")" << std::endl;
  else
  {
    std::cout << "Commanded RTL." << std::endl;
    flying = false;
  }
}

DroneInfo Drone::GetInfo() const {
  auto pos = drone->telemetry->position();
  return {pos.latitude_deg, pos.longitude_deg, pos.relative_altitude_m};
}

//////////////////////////////////////////////////////
//                  GESTION DES ERREURS             //
//////////////////////////////////////////////////////

inline void handle_action_err_exit(Action::Result result, const std::string& message)
{
  if (result != Action::Result::SUCCESS) {
    std::cerr << ERROR_CONSOLE_TEXT << message << Action::result_str(result)
              << NORMAL_CONSOLE_TEXT << std::endl;
    exit(EXIT_FAILURE);
  }
}

inline void handle_mission_err_exit(Mission::Result result, const std::string& message)
{
  if (result != Mission::Result::SUCCESS) {
    std::cerr << ERROR_CONSOLE_TEXT << message << Mission::result_str(result)
              << NORMAL_CONSOLE_TEXT << std::endl;
    exit(EXIT_FAILURE);
  }
}

inline void handle_connection_err_exit(ConnectionResult result, const std::string& message)
{
  if (result != ConnectionResult::SUCCESS) {
    std::cerr << ERROR_CONSOLE_TEXT << message << connection_result_str(result)
              << NORMAL_CONSOLE_TEXT << std::endl;
    exit(EXIT_FAILURE);
  }
}

}
