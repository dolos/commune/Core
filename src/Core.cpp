/*
** EPITECH PROJECT, 2018
** Core
** File  description:
** 
*/

#include <future>
#include <Dolos/ThreadPool/DolosThreadPool.hpp>
#include <Dolos/ModuleLoader/DolosModuleLoader.hpp>
#include <Dolos/Logger/DolosLogger.hpp>
#include <Dolos/Core.hpp>
#include <Dolos/NetworkManager.hpp>
#include <Dolos/Packets/AddWaypointPacket.hpp>
#include <Dolos_SDK/Network/INetworkClient.hpp>
#include <Dolos/Packets/AuthPacket.hpp>
#include <Dolos/Api/APIImplem.hpp>

using dolos::network::ClientConnectedEvent;
using dolos::network::ClientDisconnectedEvent;
using dolos::network::PacketReceivedEvent;

// Core specialized implementation
bool dolos::Core::init()
{
    // Packet initialisation
    getNetworkManager().registerInstantiater(DOLOS_PACKET_ADD_WAYPOINT_NAME, [](const std::string &data){
        return std::make_shared<AddWaypointPacket>(data);
    });
    getEventManager().subscribe<PacketReceivedEvent>(DOLOS_EVENT_PACKET_RECEIVED, [this](PacketReceivedEvent& event){
      if (auto p = dynamic_cast<AddWaypointPacket*>(event.getPacket().get()))
          getDrone().Goto(p->lan, p->lon, p->alt, 0);
    });

    getEventManager().subscribe<ClientConnectedEvent>(DOLOS_EVENT_CONNECTION, [this](ClientConnectedEvent &event) {
        std::cout << "New connection from client" << std::endl;
		_apiService->unregisterAvailable([this](bool result) {
			if (!result)
				std::cerr << "Unable to unregister drone from API" << std::endl;
			else
				std::cout << "Successfully unregistered drone from API" << std::endl;
			_registered = false;
		}, [] () {});
        getDrone().Init(event.getClient());
        getDrone().Takeoff(20);
        std::cout << "Client authenticated" << std::endl;
    });
    getEventManager().subscribe<ClientDisconnectedEvent>(DOLOS_EVENT_DISCONNECTION, [this](ClientDisconnectedEvent &event) {
        std::cout << "Client disconnected" << std::endl;
		_apiService->registerAvailable([this](bool success) {
			if (!success)
			{
				std::cerr << "Unable to register drone as available" << std::endl;
				return;
			}
			std::cout << "Successfully registered drone as available" << std::endl;
			_registered = true;
		}, []() { std::cerr << "An error occured while registering drone as available"; });
        getDrone().RTL();
    });

    std::cout << "Initializing threads" << std::endl;
	DolosThreadPool::Instance().init();
	_drone = std::make_shared<Drone>();
	std::cout << "Initializing modules" << std::endl;
    DolosModuleLoader::Instance().load();
	if (!dolos::network::NetworkManager::_initialized)
	{
		std::cout << "Unable to open server connection" << std::endl;
		this->release();
		return false;
	}
	std::cout << "Logging in " << std::endl;
	_apiService = std::make_unique<dolos::RestClientService>(*this);
	if (!_apiService->initialize(API_URL))
		return false;
	_apiService->loginDrone([this](bool logged) {
		if (!logged)
		{
			std::cerr << "Error: Unable to log in API" << std::endl;
			return;
		}
		std::cout << "Successfully logged in" << std::endl;
		_apiService->registerAvailable([this](bool success) {
			if (!success)
			{
				std::cerr << "Unable to register drone as available" << std::endl;
				return;
			}
			std::cout << "Successfully registered drone as available" << std::endl;
			_registered = true;
		}, []() { std::cerr << "An error occured while registering drone as available"; });
	}, [](){
		std::cerr << "An error occured while logging in API" << std::endl;
	});
	return true;
}

void dolos::Core::loop()
{
	std::chrono::time_point<std::chrono::system_clock> lastTime = std::chrono::system_clock::now();

	while (_run)
	{
	    dolos::DolosModuleLoader::Instance().onEachFrame();
		DolosThreadPool::Instance().pullMainQueue();
		getDrone().Update();
		auto now = std::chrono::system_clock::now();
		auto diff = std::chrono::duration_cast<std::chrono::milliseconds>(now - lastTime).count() / 1000; // diff in seconds
		if (diff > PING_TIMELAP)
		{
			_apiService->ping();
			lastTime = std::chrono::system_clock::now();
		}
		// Cooldown cpu usage for now
		std::this_thread::sleep_for(std::chrono::milliseconds(250));
	}
}

void dolos::Core::release()
{
	DolosModuleLoader::Instance().release();
	if (_registered) {
		std::promise<void> p;
		std::future<void> f = p.get_future();
		_apiService->unregisterAvailable([this, &p](bool result) {
			if (!result)
				std::cerr << "Unable to unregister drone from API" << std::endl;
			else
				std::cout << "Successfully unregistered drone from API" << std::endl;
			_apiService.release();
			_registered = false;
			p.set_value();
		}, [] () {});
		f.wait();
	}
	if (_drone)
		getDrone().RTL();
	_run = false;
	DolosThreadPool::Instance().release();
}

dolos::SystemLoader &dolos::Core::getSystemLoader()
{
	return dolos::SystemLoader::Instance();
}

// ICore implementation

dolos::IModuleLoader &dolos::Core::getModuleLoader()
{
	return dolos::DolosModuleLoader::Instance();
}

dolos::IRessourcesManager &dolos::Core::getRessourcesManager()
{
	if (this->getSystemLoader().getRessourcesManager() == nullptr)
		throw std::logic_error("RessourcesManager is not set");
	return *(this->getSystemLoader().getRessourcesManager());
}

dolos::Ilogger &dolos::Core::getLogger()
{
	return dolos::DolosLogger::Instance();
}

dolos::IThreadpool &dolos::Core::getThreadPool()
{
	return dolos::DolosThreadPool::Instance();
}

dolos::Drone& dolos::Core::getDrone() {
  return *_drone.get();
}

dolos::network::INetworkManager &dolos::Core::getNetworkManager() {
    return (*_networkManager);
}

dolos::Core::Core() {
    dolos::setCore(this);
    _networkManager = std::make_shared<dolos::network::NetworkManager>();
}

dolos::Core::~Core() {
    _networkManager = nullptr;
    dolos::setCore(nullptr);
}
