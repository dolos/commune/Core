/*
** EPITECH PROJECT, 2018
** spdlog
** File description:
** Logger
*/
#include "Dolos/Logger/DolosLogger.hpp"

dolos::DolosLogger::DolosLogger()
{
	struct stat info;

	if( stat(FILENAME, &info) != 0 )
		mkdir("./logs", RIGHTS_REPO);
	_console = spd::stdout_color_mt("DOLOS_OUT");
	_console->set_level(spdlog::level::trace);
}

void dolos::DolosLogger::Log(const std::string &message,dolos::TYPE_LEVEL level, dolos::TYPE_LOGGING out)
{
	switch(level)
	{
		case dolos::TYPE_LEVEL::ERROR:
			Error(message,out);
			break;
		case dolos::TYPE_LEVEL::DEBUG:
            Debug(message,out);
			break;
		case dolos::TYPE_LEVEL::INFO:
			Info(message,out);
			break;
	}
}

dolos::Ilogger &dolos::DolosLogger::Instance()
{
	static DolosLogger logger;
	return (logger);
}

void dolos::DolosLogger::Error(const std::string &message, dolos::TYPE_LOGGING out)
{
	_console->error(message);
}

void dolos::DolosLogger::Debug(const std::string &message, dolos::TYPE_LOGGING out)
{
	_console->debug(message);
}

void dolos::DolosLogger::Info(const std::string &message, dolos::TYPE_LOGGING out)
{
	_console->info(message);
}
