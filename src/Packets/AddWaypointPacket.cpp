//
// Created by mathias on 24.04.19.
//

#include <iostream>
#include <Dolos/Packets/AddWaypointPacket.hpp>
#include <sstream>
#include <vector>
#include <string>
#include <algorithm>

std::string dolos::AddWaypointPacket::getName() const {
    return DOLOS_PACKET_ADD_WAYPOINT_NAME;
}

// NOT USED, THIS PACKET IS ONLY RECEIVED AND NOT SENT
std::string dolos::AddWaypointPacket::serialize() const {
    return "";
}

dolos::AddWaypointPacket::AddWaypointPacket(const std::string &data) {
    std::cout << "deserialized" << std::endl;
    std::stringstream ss(data);

    std::string coord(data);
    std::replace(coord.begin(), coord.end(), ',', '.');
    std::vector<double> out;

    size_t start;
	size_t end = 0;
	while ((start = coord.find_first_not_of(':', end)) != std::string::npos)
	{
		end = coord.find(':', start);
		out.push_back(std::stod(coord.substr(start, end - start)));
	}

    lan = out[0];
    lon = out[1];
    alt = out[2];
}
