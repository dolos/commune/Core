/**
 * @author Mathias Hyrondelle
 * API Service implementation using restclient-cpp
 */
#include "Dolos/Api/APIImplem.hpp"
#include "rapidjson/document.h"
#include <sstream>
#include <iostream>
namespace dolos {

    RestClientService::RestClientService(Core &core) : _core(core)
    {
        
    }

    bool RestClientService::initialize(const std::string api_url)
    {
        int result = RestClient::init();
        std::ostringstream stream;
        _api_url = api_url;
        stream << api_url << LOGIN_ROUTE;
        _login_route = stream.str();
        stream.clear();
        return true;
    }

    void RestClientService::release() {
        RestClient::disable();
    }

    void RestClientService::loginDrone(Callback<bool> onResponse, VoidCallback onError)
    {
        
        _core.getThreadPool().push([this, onResponse, onError](){
            try {
                RestClient::Response response = RestClient::post(_login_route, "application/json", "{\"name\": \"drone\", \"password\": \"dolos\"}");
                if (response.code == 200)
                {
                    rapidjson::Document document;
                    document.Parse(response.body.c_str());
                    _token = document["token"].GetString();
                }
                _core.getThreadPool().executeOnMainThread([onResponse, response] () { onResponse(response.code == 200); });
            } catch (std::exception e)
            {
                std::cerr << e.what() << std::endl;
                _core.getThreadPool().executeOnMainThread(onError);
            }
        }, [](){});
    }
    
    void RestClientService::registerAvailable(Callback<bool> onResponse, VoidCallback onError)
    {
        _core.getThreadPool().push([this, onResponse, onError]() {
            try {
                RestClient::Connection *connection = this->getConnection();
                RestClient::Response r = connection->get(AVAILABLE_ROUTE);
                delete connection;
                _core.getThreadPool().executeOnMainThread([onResponse, r] () {onResponse(r.code == 200); });
            } catch (std::exception e)
            {
                std::cerr << e.what() << std::endl;
                _core.getThreadPool().executeOnMainThread(onError);
            }
        }, []() {});
    }

    void RestClientService::unregisterAvailable(Callback<bool> onResponse, VoidCallback onError) {
        _core.getThreadPool().push([this, onResponse, onError]() {
            try {
                RestClient::Connection *connection = this->getConnection();
                RestClient::Response r = connection->get(NOT_AVAILABLE_ROUTE);
                delete connection;
                _core.getThreadPool().executeOnMainThread([onResponse, r] () {onResponse(r.code == 200); });
            } catch (std::exception e)
            {
                std::cerr << e.what() << std::endl;
                _core.getThreadPool().executeOnMainThread(onError);
            }
        }, []() {});
    }

    void RestClientService::ping()
    {
        if (_ping) return;
        _ping = true;
        _core.getThreadPool().push([this] () {
            try {
                RestClient::Connection *connection = this->getConnection();
                RestClient::Response r = connection->get(PING_ROUTE);
                if (r.code != 200)
                    std::cout << "Failed ping: " << r.code << std::endl;
                delete connection;
            } catch (std::exception e)
            {
                std::cerr << e.what() << std::endl;
            }
        },
        [this] () {
            _ping = false;
        });
    }

    RestClient::Connection *RestClientService::getConnection() const {
        RestClient::Connection *connection = new RestClient::Connection(_api_url);
        connection->SetTimeout(15);
        connection->FollowRedirects(true);
        RestClient::HeaderFields headers;
        headers["Accept"] = "*/*";
        headers["Content-Type"] = "application/json";
        std::ostringstream stream;
        stream << "Bearer " << _token;
        std::string bearer = stream.str();
        headers["Authorization"] = bearer;
        connection->SetHeaders(headers);
        return connection;
    }
}